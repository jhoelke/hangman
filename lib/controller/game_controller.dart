import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hangman/core/hangman.dart';
import 'package:hangman/core/statistics.dart';

/// Controller class for the Hangman game
/// @author: johannes.hoelken@hawk.de
class GameController {

  final Statistics stats = Statistics();

  Hangman game;

  List<String> get misses => game?.result?.misses;

  List<String> get word => game?.result?.word;

  bool get solved => game?.result?.solved;

  /// Set the [game] to control
  void setGame(Hangman game) {
    this.game = game;
  }

  /// Make a guess for the given [char]
  void guess(String char) {
    if (game == null) {
      return;
    }

    game.guess(char);
    if(finished()) {
      stats.count(game.totalGuesses, game.currentMisses, won: solved);
    }
  }

  /// Check if the [char] has already been used in this game
  bool available(String char) {
    if (game == null || game.finished) {
      return false;
    }
    final String test = char.toUpperCase();
    return !(misses.contains(test) || word.contains(test));
  }

  /// Check if the new game button is to be displayed
  bool showNewGameButton() {
    if (game == null) {
      return true;
    }
    return game.finished;
  }

  /// Check if the game is finished
  bool finished() {
    if (game == null) {
      return false;
    }
    return game.finished;
  }

  /// Get the number of misses currently done
  int numMisses() {
    if (game == null) {
      return 0;
    }
    return game.currentMisses;
  }

  /// Generate the current status of the word to guess with underlines for missing characters
  String guessWordString() {
    String text = game?.result?.word?.join(' ');
    text ??= '';
    return text;
  }

  /// Generate info message of used wrong characters
  String missesText() {
    final List<String> misses = game?.result?.misses;
    if (misses == null) {
      return '';
    }

    return 'Falsch (${numMisses()}/${game.maxMisses}): ${misses.join(', ')}';
  }

  Widget finishOverlay(BuildContext context) {
    if(game == null) {
      return null;
    }

    return Material(
      elevation: 4.0,
      color: solved ? Colors.lightGreen : Colors.redAccent,
      child: Column(
        children: <Widget>[
          const Center(
            child: Text(''),
          ),
          Center(
            child: Text(
              solved ? 'Du hast Gewonnen!' : 'Du hast Verloren!',
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          Center(
            child: Text('Gesucht war "${game.solution}".'),
          ),
        ],
      ),
    );
  }


}
