import 'dart:convert';
import 'dart:math';

import 'package:flutter/services.dart' show rootBundle;


/// Database provides access to a word list.
class Database {

  String deList = 'wortlist.txt';
  Random generator = Random();
  List<String> words;

  Database({String lang = 'de'}) {
    if(lang == 'de') {
      _load(deList);
    }
    else {
      throw Exception('Lang $lang currently not supported.');
    }
  }

  Future<String> loadAsset(String listName) async {
    return await rootBundle.loadString('assets/db/$listName');
  }

  void _load(String listName) {
    loadAsset(listName).then((String text) => <void>{
      words = const LineSplitter().convert(text)
    });
  }

  /// Return a random word from the loaded database
  String randomWord() {
    if(words == null || words.isEmpty) {
      throw StateError('Database not initialized');
    }
    return words[generator.nextInt(words.length)];
  }

}