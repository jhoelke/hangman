import 'package:hangman/core/result.dart';

/// Business Logic for the hangman game
class Hangman {

  String _word;
  int _maxMisses;
  Result result;

  Hangman(String word, {int maxMisses = 11}) {
    _word = word.toUpperCase();
    _maxMisses = maxMisses;
    result = Result(word.length);
  }

  int get maxMisses => _maxMisses;
  int get currentMisses => result.numMisses;
  int get totalGuesses => result.totalGuesses;
  bool get finished => _maxMisses <= currentMisses || result.solved;
  String get solution => _word;

  /// Make a guess
  Result guess(String input) {
    _checkGuessAllowed();

    final String guess = _extractGuess(input);
    if(!_markMatches(guess)) {
      _markMiss(guess);
    }
    return result;
  }

  void _checkGuessAllowed() {
    if(_maxMisses <= currentMisses) {
      throw OutOfGuesses('No guesses left, you had $currentMisses of $maxMisses wrong guesses.');
    }
  }

  String _extractGuess(String input) {
    return input.trimLeft()[0].toUpperCase();
  }

  bool _markMatches(String guess) {
    bool found = false;
    int idx = _word.indexOf(guess);
    while ( idx > -1 && idx < _word.length ) {
      result.addScore(idx, guess);
      idx = _word.indexOf(guess, idx+1);
      found = true;
    }
    return found;
  }

  void _markMiss(String guess) {
    result.addMiss(guess);
  }
}

class OutOfGuesses implements Exception {
  String message;
  OutOfGuesses(this.message);
}