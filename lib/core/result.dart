/// Result class to store the already guessed word parts.
class Result {
  List<String> word;
  final List<String> _misses = <String>[];

  Result(int length) {
    word = List<String>.filled(length, '_');
  }

  // getter
  int get length => word.length;
  bool get solved => !word.contains('_');
  int get numMisses => _misses.length;
  List<String> get misses => _misses..sort();

  int get totalGuesses {
    return word.length - word.where((String c) => c == '_').length + _misses.length;
  }

  /// Check if the given character was already used.
  bool alreadyGuessed(String char) {
    final String test = char.toUpperCase();
    if (word.contains(test)) {
      return true;
    }

    return _misses.contains(test);
  }

  /// Add a missed character to the result
  void addMiss(String char) {
    final String miss = char.toUpperCase();
    if (_misses.contains(miss)) {
      return;
    }

    _misses.add(miss);
  }

  /// Add a scored character to the result
  void addScore(int pos, String char) {
    word[pos] = char.toUpperCase();
  }
}
