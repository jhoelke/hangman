import 'package:shared_preferences/shared_preferences.dart';

/// Statistics provider for the game
/// Stores stats in simple key/value manner on the device
/// @author: johannes.hoelken@hawk.de
class Statistics {

  static final Statistics _instance = Statistics._internal();

  factory Statistics() {
    return _instance;
  }

  Statistics._internal(){
    connect();
  }

  Future<void> connect() async {
    _prefs = await SharedPreferences.getInstance();
  }

  Future<Map<String,double>> getStatsAsync() async {
    _prefs ??= await SharedPreferences.getInstance();
    final Map<String,double> result = <String,double>{};
    result[totalKey] = total;
    result[avrgErrorKey] = avrgError;
    result[avrgGuessesKey] = avrgGuesses;
    result[seriesKey] = series;
    result[longestSeriesKey] = longestSeries;
    return result;
  }

  static String totalKey = 'total';
  static String avrgErrorKey = 'avrgError';
  static String avrgGuessesKey = 'avrgGuesses';
  static String seriesKey = 'series';
  static String longestSeriesKey = 'longestSeries';
  final List<String> _keys = <String>[totalKey, avrgErrorKey, avrgGuessesKey, seriesKey, longestSeriesKey];
  SharedPreferences _prefs;

  List<String> get keys => List<String>.from(_keys);
  double get total => _prefs.getDouble(totalKey) ?? 0;
  double get avrgGuesses => _prefs.getDouble(avrgGuessesKey) ?? 0;
  double get avrgError => _prefs.getDouble(avrgErrorKey) ?? 0;
  double get series => _prefs.getDouble(seriesKey) ?? 0;
  double get longestSeries => _prefs.getDouble(longestSeriesKey) ?? 0;

  /// Reset all statistics to zero.
  void reset() {
    for(final String key in _keys) {
      _prefs.setDouble(key, 0);
    }
  }

  /// add the current game to the statistics
  void count(int guesses, int errors, {bool won = true}) {
    _prefs.setDouble(totalKey, total + 1);
    _prefs.setDouble(avrgErrorKey, _rollingAverage(avrgErrorKey, errors));
    _prefs.setDouble(avrgGuessesKey, _rollingAverage(avrgGuessesKey, guesses));
    if(won) {
      final double newVal = series + 1;
      _prefs.setDouble(seriesKey, newVal);
      if(longestSeries < newVal) {
        _prefs.setDouble(longestSeriesKey, newVal);
      }
    }
    else {
      _prefs.setDouble(seriesKey, 0);
    }
  }

  /// calculate the average from the previous value
  /// Ensure that [totalKey] is increased before.
  double _rollingAverage(String key, int newVal) {
    final double prevMean = _prefs.getDouble(key) ?? 0;
    return prevMean + (newVal - prevMean) / total;
  }

}