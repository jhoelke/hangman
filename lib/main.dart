import 'package:flutter/material.dart';
import 'package:hangman/pages/home.dart';
import 'package:hangman/pages/statistics.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Galgenmännchen',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      // ignore: always_specify_types
      routes: {
        '/': (BuildContext context) => const MyHomePage(title: 'Galgenmännchen'),
        '/stats': (BuildContext context) => const MyStats(title: 'Statistiken'),
      },
      //home: const MyHomePage(title: 'Galgenmännchen'),
    );
  }
}


