import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:hangman/controller/game_controller.dart';
import 'package:hangman/core/database.dart';
import 'package:hangman/core/hangman.dart';
import 'package:hangman/utils/ui_helper.dart';

/// Home page widget
/// @author: johannes.hoelken@hawk.de
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.database, this.title, this.maxMisses})
      : super(key: key);

  final String title;
  final int maxMisses;
  final Database database;

  @override
  _MyHomePageState createState() =>
      _MyHomePageState(database: database, maxMisses: maxMisses);
}

/// Home page state
/// @author: johannes.hoelken@hawk.de
class _MyHomePageState extends State<MyHomePage> {
  final GameController _gameController = GameController();

  int _maxMisses;
  Database _database;
  OverlayEntry _overlayEntry;

  /// Constructor that allows optional dependency injection of [database] and [maxMisses].
  /// If not given the defaults will be used.
  _MyHomePageState({Database database, int maxMisses}) {
    _database = database ?? Database(lang: 'de');
    _maxMisses = maxMisses ?? 10;
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage(
                  'assets/hangman/${_gameController.numMisses()}.png'),
            ),
          ),
          const Center(
            child: Text(''),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  _gameController.guessWordString(),
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
          ),
          const Center(
            child: Text(''),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  _gameController.missesText(),
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ],
            ),
          ),
          const Center(
            child: Text(''),
          ),
          Expanded(
            child: GridView.count(
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
              crossAxisCount: 6,
              children: _alphabet(),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomLeft,
                child: Visibility(
                  child: FloatingActionButton(
                    heroTag: 'stats',
                    backgroundColor: Colors.green,
                    child: const Text('Stats'),
                    tooltip: 'show statistics',
                    onPressed: _goToStats,
                  ),
                  visible: _gameController.showNewGameButton(),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Visibility(
                  child: FloatingActionButton(
                    heroTag: 'new',
                    onPressed: _newGame,
                    tooltip: 'Neues Spiel starten',
                    child: const Text('Neu'),
                  ),
                  visible: _gameController.showNewGameButton(),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _newGame() {
    setState(() {
      _gameController.setGame(Hangman(_database.randomWord(), maxMisses: _maxMisses));
      _closeOverlay();
    });
  }

  void _goToStats() {
    _closeOverlay();
    Navigator.pushNamed(context, '/stats');
  }

  void _closeOverlay() {
    if (_overlayEntry != null) {
      _overlayEntry.remove();
      _overlayEntry = null;
    }
  }

  void _guess(String char) {
    setState(() {
      _gameController.guess(char);
      if (_gameController.finished()) {
        _overlayEntry = _createOverlayEntry();
        Overlay.of(context).insert(_overlayEntry);
      }
    });
  }

  List<FlatButton> _alphabet() {
    return UiHelper.buttonAlphabet(_gameController, _guess);
  }

  OverlayEntry _createOverlayEntry() {
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Size size = renderBox.size;

    return OverlayEntry(
        builder: (BuildContext context) => Positioned(
              top: 210.0,
              height: 90.0,
              width: size.width,
              child: _gameController.finishOverlay(context),
            ));
  }
}
