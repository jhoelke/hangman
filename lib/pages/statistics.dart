import 'package:flutter/material.dart';
import 'package:hangman/core/statistics.dart';

/// Home page widget
class MyStats extends StatefulWidget {
  const MyStats({Key key, this.title, this.stats}) : super(key: key);

  final String title;
  final Statistics stats;

  @override
  _MyStatsState createState() => _MyStatsState(statsProvider: stats);
}

/// Home page state
class _MyStatsState extends State<MyStats> {

  Statistics stats;

  _MyStatsState({Statistics statsProvider}) {
    stats = statsProvider ?? Statistics();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder<Map<String,double>>(
        future: stats.getStatsAsync(), // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<Map<String,double>> snapshot) {
          List<Widget> children;
          if (snapshot.hasData) {
            children = <Widget>[
              Center(
                child: Table(
                  children: _gatherStats(snapshot.data),
                ),
              ),
            ];
          } else if (snapshot.hasError) {
            children = <Widget>[
              const Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${snapshot.error}'),
              )
            ];
          } else {
            children = <Widget>[
              const SizedBox(
                child: CircularProgressIndicator(),
                width: 60,
                height: 60,
              ),
              const Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text('Awaiting result...'),
              )
            ];
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: children,
            ),
          );
        }
      ),
      floatingActionButton:  FloatingActionButton(
        onPressed: _reset,
        tooltip: 'Alles Löschen',
        backgroundColor: Colors.red,
        child: const Text('Reset'),
      ),
    );
  }

  void _reset() {
    setState(() {
      stats.reset();
    });
  }

  List<TableRow> _gatherStats(Map<String,double> data) {
    final List<TableRow> rows = <TableRow>[];
    final TextStyle style = Theme.of(context).textTheme.headline6;
    rows.add(
      TableRow(
        children: <Widget>[
          Text('Total', style: Theme.of(context).textTheme.headline4),
          Text('${data[Statistics.totalKey]}', style: Theme.of(context).textTheme.headline4)
        ],
      ),
    );
    rows.add(
      TableRow(
        children: <Widget>[
          Text('Fehler Ø', style: style),
          Text('${data[Statistics.avrgErrorKey]}', style: style)
        ],
      ),
    );
    rows.add(
      TableRow(
        children: <Widget>[
          Text('Aktuelle Siegesserie', style: style),
          Text('${data[Statistics.seriesKey]}', style: style)
        ],
      ),
    );
    rows.add(
      TableRow(
        children: <Widget>[
          Text('Längste Siegesserie', style: style),
          Text('${data[Statistics.longestSeriesKey]}', style: style)
        ],
      ),
    );
    return rows;
  }
}
