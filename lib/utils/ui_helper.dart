import 'package:flutter/material.dart';
import 'package:hangman/controller/game_controller.dart';

class UiHelper {

  static List<FlatButton> buttonAlphabet(GameController controller, Function onPressCallback) {
    final List<FlatButton> buttons = <FlatButton>[];
    for (int c = 'a'.codeUnitAt(0); c <= 'z'.codeUnitAt(0); c++) {
      final String char = String.fromCharCode(c).toUpperCase();

      void _onPress() {
        onPressCallback(char);
      }

      buttons.add(FlatButton(
        color: Colors.blue,
        textColor: Colors.white,
        disabledColor: Colors.grey,
        disabledTextColor: Colors.black,
        padding: const EdgeInsets.all(8.0),
        splashColor: Colors.blueAccent,
        onPressed: controller.available(char) ? _onPress : null,
        key: Key('button$char'),
        child: Text(
          char,
        ),
      ));
    }
    return buttons;
  }

}