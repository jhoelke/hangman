import 'package:flutter_test/flutter_test.dart';

import 'helper/mock_classes.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group('Database', () {
    final DatabaseMock db = DatabaseMock();

    test('Should load database by default', () {
      expect(db.words.length, 3);
    });

    test('Should return random word', () {
      final String word = db.randomWord();
      expect(word, isA<String>());
      expect(word.length == 5, isTrue);
      expect(word.startsWith('Test'), isTrue);
    });
  });
}