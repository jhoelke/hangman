import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:hangman/controller/game_controller.dart';
import 'package:hangman/core/hangman.dart';

import 'helper/setup.dart';

void main() {
  group('Controller', () {
    group('GameController', () {
      TestWidgetsFlutterBinding.ensureInitialized();
      Setup.statistics();

      test('it should be null save', () {
        final GameController controller = GameController();
        expect(() => controller.guess(''), returnsNormally);

        expect(() => controller.available(''), returnsNormally);
        expect(controller.available(''), isFalse);

        expect(() => controller.showNewGameButton(), returnsNormally);
        expect(controller.showNewGameButton(), isTrue);

        expect(() => controller.finished(), returnsNormally);
        expect(controller.finished(), isFalse);

        expect(() => controller.numMisses(), returnsNormally);
        expect(controller.numMisses(), equals(0));

        expect(() => controller.guessWordString(), returnsNormally);
        expect(controller.guessWordString(), equals(''));

        expect(() => controller.missesText(), returnsNormally);
        expect(controller.missesText(), equals(''));

        expect(controller.solved, isNull);
        expect(controller.word, isNull);
        expect(controller.misses, isNull);
      });

      test('it should take a game', () {
        final GameController controller = GameController();
        expect(() => controller.setGame(Hangman('foo')), returnsNormally);
      });

      test('it should provide the current solution of the word', () {
        final GameController controller = GameController();
        controller.setGame(Hangman('foo'));
        expect(controller.guessWordString(), equals('_ _ _'));

        controller.guess('f');
        expect(controller.guessWordString(), equals('F _ _'));

        controller.setGame(Hangman('foobar'));
        expect(controller.guessWordString(), equals('_ _ _ _ _ _'));
      });

      test('it should know if the game is finished', () {
        final GameController controller = GameController();
        controller.setGame(Hangman('a'));
        controller.guess('a');
        expect(controller.finished(), isTrue);
        expect(controller.showNewGameButton(), isTrue);

        controller.setGame(Hangman('a', maxMisses: 1));
        controller.guess('b');
        expect(controller.finished(), isTrue);
        expect(controller.showNewGameButton(), isTrue);
      });

      test('it should provide a text message of wrong guesses', () {
        final GameController controller = GameController();
        controller.setGame(Hangman('a', maxMisses: 2));
        expect(controller.missesText(), equals('Falsch (0/2): '));

        controller.guess('b');
        expect(controller.missesText(), equals('Falsch (1/2): B'));

        controller.guess('c');
        expect(controller.missesText(), equals('Falsch (2/2): B, C'));
      });

      test('it should know if a character has benn used', () {
        final GameController controller = GameController();
        controller.setGame(Hangman('abc'));

        expect(controller.available('A'), isTrue);
        expect(controller.available('a'), isTrue);
        controller.guess('a');
        expect(controller.available('A'), isFalse);
        expect(controller.available('a'), isFalse);

        expect(controller.available('X'), isTrue);
        expect(controller.available('x'), isTrue);
        controller.guess('x');
        expect(controller.available('X'), isFalse);
        expect(controller.available('x'), isFalse);
      });

      testWidgets('it should create the finish overlay content', (WidgetTester tester) async {
        await tester.pumpWidget(
          Builder(
            builder: (BuildContext context) {
              final GameController controller = GameController();
              expect(() => controller.finishOverlay(context), returnsNormally);

              controller.setGame(Hangman('a'));
              controller.guess('a');
              expect(() => controller.finishOverlay(context), returnsNormally);

              // The builder function must return a widget.
              return const Placeholder();
            },
          ),
        );
      });
    });
  });
}