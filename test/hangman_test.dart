import 'package:flutter_test/flutter_test.dart';

import 'package:hangman/core/hangman.dart';

void main() {
  group('Hangman', () {
    group('Initialize', () {
      test('Default max misses should be 11', () {
        expect(Hangman('Foo').maxMisses, 11);
      });
      test('Max misses should be configurable', () {
        expect(Hangman('Foo', maxMisses: 10).maxMisses, 10);
      });
      test('Result length should match word length', () {
        expect(Hangman('Foo').result.length, 3);
        expect(Hangman('test').result.length, 4);
        expect(Hangman('FooBarBaz').result.length, 9);
      });
      test('No misses should be recorded per default', () {
        expect(Hangman('Foo').currentMisses, 0);
      });
      test('Not finished by default', () {
        expect(Hangman('Foo').finished, isFalse);
      });
    });
    group('Guess', () {
      test('Correct guesses are recorded on the corresponding locations', () {
        final Hangman game = Hangman('FooBarBaz');

        game.guess('O');
        expect(game.result.word, <String>['_', 'O', 'O', '_', '_', '_', '_', '_', '_']);

        game.guess('b');
        expect(game.result.word, <String>['_', 'O', 'O', 'B', '_', '_', 'B', '_', '_']);
      });
      test('Incorrect guesses are recorded', () {
        final Hangman game = Hangman('foo');

        game.guess('e');
        expect(game.result.word, <String>['_', '_', '_']);
        expect(game.result.misses, <String>['E']);
        expect(game.currentMisses, 1);
      });
      test('Wrong guesses are counted uniquely', () {
        final Hangman game = Hangman('foo');
        game.guess('e');
        expect(game.currentMisses, 1);
        game.guess('e');
        expect(game.currentMisses, 1);
      });
      test('Guesses are no longer accepted once number of max wrong guesses is exceeded.', () {
        final Hangman game = Hangman('foo', maxMisses: 2);
        game.guess('x');
        expect(game.currentMisses, 1);
        game.guess('y');
        expect(game.currentMisses, 2);

        try {
          game.guess('z');
          throw TestFailure('Expected exception not observed.');
        }
        on OutOfGuesses catch(e) {
          expect(e, isA<OutOfGuesses>());
          expect(e.message.contains('2 of 2 wrong guesses'), isTrue);
          expect(game.finished, isTrue);
        }
      });
    });
  });
}
