import 'package:flutter_test/flutter_test.dart';
import 'package:collection/collection.dart';

class Callbacks {

  static final Callbacks _instance = Callbacks._internal();

  factory Callbacks() {
    return _instance;
  }

  Callbacks._internal();

  List<Object> singleArgs = <Object>[];

  static void reset() {
    _instance.singleArgs = <Object>[];
  }

  static void singleArg(Object arg) {
    _instance.singleArgs.add(arg);
  }

  static void assertCalled(String name, {int times, List<Object> withArgs}) {
    if (name == 'singleArg') {
      _instance.assertTimes(name, times, _instance.singleArgs.length);
      _instance.assertSingleArguments(withArgs);
      return;
    }

    throw ArgumentError('Callback method "$name" unknown.');
  }

  void assertTimes(String name, int expected, int actual) {
    if (expected == null || actual == expected) {
      return;
    }

    throw TestFailure(
        'Expected "$name" was called $expected times, but counted $actual');
  }

  void assertSingleArguments(List<Object> expected) {
    if (expected == null) {
      return;
    }
    if (const ListEquality<Object>().equals(expected, _instance.singleArgs)) {
      return;
    }

    throw TestFailure(
        'Expected Arguments for "singleArg" not observed. \nExpected: ${expected
            .join(', ')}\nActual: ${_instance.singleArgs.join(', ')}');
  }

}