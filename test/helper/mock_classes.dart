import 'dart:async';

import 'package:hangman/core/statistics.dart';
import 'package:mockito/mockito.dart';

import 'package:hangman/controller/game_controller.dart';

import 'package:hangman/core/database.dart';

class GameControllerMock extends Mock implements GameController {}

class StatisticsMock extends Mock implements Statistics {}

/// Test implementation of Database to
/// avoid FileIO in unit tests
class DatabaseMock extends Database {
  @override
  Future<String> loadAsset(String listName) async {
    final Completer<String> completer = Completer<String>();
    completer.complete('Test1\nTest2\nTest3');
    return completer.future;
  }
}