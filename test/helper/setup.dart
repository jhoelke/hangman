import 'package:flutter/services.dart';
import 'package:hangman/core/statistics.dart';

/// Helper class for recurring setup activities
class Setup {

   static void statistics() {
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });

    Statistics();
  }

}