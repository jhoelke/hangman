import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hangman/core/database.dart';

import 'package:hangman/pages/home.dart';

import 'helper/mock_classes.dart';
import 'helper/setup.dart';

void main() {
  group('Home', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    Setup.statistics();

    final Database db = DatabaseMock();

    testWidgets('HomePage has a title', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(title: 'Base', home: MyHomePage(title: 'Title', database: db)));
      expect(find.text('Title'), findsOneWidget);
    });

    testWidgets('it should start a game when tapping new', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(title: 'Base', home: MyHomePage(title: 'Title', database: db)));

      await tester.tap(find.byTooltip('Neues Spiel starten'));

      // Rebuild the widget after the state has changed.
      await tester.pump();

      expect(find.text('_ _ _ _ _'), findsOneWidget);
    });

    testWidgets('it should take correct guesses', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(title: 'Base', home: MyHomePage(title: 'Title', database: db)));

      await tester.tap(find.byTooltip('Neues Spiel starten'));
      await tester.pumpAndSettle();

      await tester.tap(find.byKey(const Key('buttonE')));
      await tester.pumpAndSettle();

      expect(find.text('_ E _ _ _'), findsOneWidget);
    });

    testWidgets('it should take wrong guesses', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(title: 'Base', home: MyHomePage(title: 'Title', database: db)));

      await tester.tap(find.byTooltip('Neues Spiel starten'));
      await tester.pumpAndSettle();

      await tester.tap(find.byKey(const Key('buttonH')));
      await tester.pumpAndSettle();

      //print(tester.elementList(find.byType(Text)).join(" || "));
      expect(find.text('_ _ _ _ _'), findsOneWidget);
      expect(find.text('Falsch (1/10): H'), findsOneWidget);
    });

    testWidgets('it should display an overlay after the game', (WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(title: 'Base', home: MyHomePage(title: 'Title', database: db, maxMisses: 1,)));

      await tester.tap(find.byTooltip('Neues Spiel starten'));
      await tester.pumpAndSettle();

      await tester.tap(find.byKey(const Key('buttonA')));
      await tester.pumpAndSettle();

      expect(find.text('Du hast Verloren!'), findsOneWidget);
    });
  });
}