import 'package:flutter_test/flutter_test.dart';

import 'package:hangman/core/result.dart';

void main() {
  group('Result', () {
    group('Initialize', () {
      test('Result should be of the given length', () {
        expect(Result(0).length, 0);
        expect(Result(1).length, 1);
        expect(Result(3).length, 3);
        expect(Result(15).length, 15);
      });

      test('Result should be initialized with underscores only', () {
        final Result result = Result(2);
        for (final String entry in result.word) {
          expect(entry, '_');
        }
      });

      test('Result should be initialized unsolved', () {
        expect(Result(1).solved, false);
        expect(Result(0).solved, true);
      });
    });
    group('Misses', () {
      test('Misses are added in upper case', () {
        final Result result = Result(0);
        result.addMiss('a');
        result.addMiss('B');
        result.addMiss('c');
        expect(result.misses, <String>['A', 'B', 'C']);
      });

      test('Misses are recorded as already known', () {
        final Result result = Result(0);
        expect(result.alreadyGuessed('a'), isFalse);
        result.addMiss('a');
        expect(result.alreadyGuessed('a'), isTrue);
      });

      test('Misses are stored alphabetically', () {
        final Result result = Result(0);
        result.addMiss('Z');
        result.addMiss('x');
        result.addMiss('a');
        expect(result.misses, <String>['A', 'X', 'Z']);
      });

      test('Misses should be counted only once', () {
        final Result result = Result(0);
        expect(result.numMisses, 0);
        expect(result.totalGuesses, 0);

        result.addMiss('a');
        expect(result.numMisses, 1);
        expect(result.totalGuesses, 1);

        result.addMiss('A');
        expect(result.numMisses, 1);
        expect(result.totalGuesses, 1);

        result.addMiss('b');
        expect(result.numMisses, 2);
        expect(result.totalGuesses, 2);
      });
    });
    group('Scores', () {
      test('Scores can be set at the given position', () {
        final Result result = Result(3);
        result.addScore(1, 'A');
        expect(result.word, <String>['_', 'A', '_']);
        expect(result.totalGuesses, 1);
      });

      test('Scores are recorded as already known', () {
        final Result result = Result(1);
        expect(result.alreadyGuessed('a'), isFalse);
        result.addScore(0, 'a');
        expect(result.alreadyGuessed('a'), isTrue);
      });

      test('Scores are added in upper case', () {
        final Result result = Result(1);
        result.addScore(0, 'u');
        expect(result.word, <String>['U']);
      });

      test('Scores can be added on multiple positions', () {
        final Result result = Result(3);
        result.addScore(0, 'a');
        result.addScore(2, 'a');
        result.addScore(1, 'b');
        expect(result.word, <String>['A', 'B', 'A']);
      });

      test('If all positions are scored it should be solved', () {
        final Result result = Result(2);
        expect(result.solved, false);
        result.addScore(0, 'a');
        expect(result.solved, false);
        result.addScore(1, 'b');
        expect(result.solved, true);
      });
    });
  });
}
