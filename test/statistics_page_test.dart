import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hangman/core/statistics.dart';

import 'package:hangman/pages/statistics.dart';
import 'package:mockito/mockito.dart';

import 'helper/mock_classes.dart';
import 'helper/setup.dart';

void main() {
  group('Statistics', () {
    TestWidgetsFlutterBinding.ensureInitialized();
    Setup.statistics();

    testWidgets('HomePage has a title', (WidgetTester tester) async {
      await tester.pumpWidget(
          const MaterialApp(title: 'Base', home: MyStats(title: 'Title')));
      expect(find.text('Title'), findsOneWidget);
    });

    testWidgets('it should show statistics', (WidgetTester tester) async {
      final Map<String, double> result = <String, double>{};
      result[Statistics.totalKey] = 0.0;
      result[Statistics.avrgErrorKey] = 0.0;
      result[Statistics.avrgGuessesKey] = 0.0;
      result[Statistics.seriesKey] = 0.0;
      result[Statistics.longestSeriesKey] = 0.0;

      final Completer<Map<String, double>> completer = Completer<Map<String, double>>();
      completer.complete(result);

      final StatisticsMock stats = StatisticsMock();
      when(stats.getStatsAsync()).thenAnswer((_) => completer.future);
      await tester.pumpWidget(MaterialApp(
          title: 'Base', home: MyStats(title: 'Title', stats: stats)));
      await tester.pumpAndSettle();

      expect(find.text('Total'), findsOneWidget);
      expect(find.text('0.0'), findsNWidgets(4));
    });
  });
}
