import 'package:flutter_test/flutter_test.dart';

import 'package:hangman/core/statistics.dart';

import 'helper/setup.dart';

void main() {
  group('core', () {
    group('Statistics', () {
      TestWidgetsFlutterBinding.ensureInitialized();
      Setup.statistics();

      final Statistics stats = Statistics();

      test('it should start with 0', () {
        stats.reset();

        expect(stats.total, 0);
        expect(stats.avrgGuesses, 0);
        expect(stats.avrgError, 0);
        expect(stats.series, 0);
        expect(stats.longestSeries, 0);
      });

      test('it should count statistics', () {
        stats.reset();

        stats.count(8, 2);
        expect(stats.total, 1);
        expect(stats.avrgGuesses, 8.0);
        expect(stats.avrgError, 2.0);
        expect(stats.series, 1);
        expect(stats.longestSeries, 1);

        stats.count(8, 2);
        expect(stats.total, 2.0);
        expect(stats.avrgGuesses, 8.0);
        expect(stats.avrgError, 2.0);
        expect(stats.series, 2.0);
        expect(stats.longestSeries, 2.0);
      });

      test('it should compute average values', () {
        stats.reset();

        stats.count(8, 2);
        expect(stats.avrgGuesses, 8.0);
        expect(stats.avrgError, 2.0);

        stats.count(10, 4);
        expect(stats.avrgGuesses, (8 + 10) / 2);
        expect(stats.avrgError, (2 + 4) / 2);

        stats.count(12, 3);
        expect(stats.avrgGuesses, (8 + 10 + 12) / 3);
        expect(stats.avrgError, (2 + 4 + 3) / 3);

        stats.count(6, 0);
        expect(stats.avrgGuesses, (8 + 10 + 12 + 6) / 4);
        expect(stats.avrgError, (2 + 4 + 3) / 4);
      });

      test('it should count winning series', () {
        stats.reset();

        stats.count(1, 0);
        expect(stats.total, 1);
        expect(stats.series, 1);
        expect(stats.longestSeries, 1);

        stats.count(1, 0);
        expect(stats.total, 2);
        expect(stats.series, 2);
        expect(stats.longestSeries, 2);

        stats.count(1, 0, won: false);
        expect(stats.total, 3);
        expect(stats.series, 0);
        expect(stats.longestSeries, 2);
      });
    });
  });
}
