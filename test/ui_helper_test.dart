import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:hangman/utils/ui_helper.dart';
import 'package:mockito/mockito.dart';

import 'helper/callbacks.dart';
import 'helper/mock_classes.dart';

void main() {
  group('Utils', () {
    group('UiHelper', () {
      group('buttonAlphabet', () {
        test('it should return 26 buttons', () {
          Callbacks.reset();
          final GameControllerMock mock = GameControllerMock();
          when(mock.available(any)).thenReturn(true);

          final List<FlatButton> buttons = UiHelper.buttonAlphabet(mock, Callbacks.singleArg);

          expect(buttons.length, 26);
          Callbacks.assertCalled('singleArg', times: 0);
          for(final FlatButton button in buttons) {
            expect(button.enabled, isTrue);
          }
        });

        test('it should disable used buttons', () {
          final GameControllerMock mock = GameControllerMock();
          when(mock.available(any)).thenReturn(true);
          when(mock.available('A')).thenReturn(false);

          final List<FlatButton> buttons = UiHelper.buttonAlphabet(mock, Callbacks.singleArg);

          for(final FlatButton button in buttons) {
            if(button.child.toString() == 'Text("A")') {
              expect(button.enabled, isFalse);
            }
            else {
              expect(button.enabled, isTrue);
            }
          }
        });

        test('it should call given callback when pressed', () {
          Callbacks.reset();
          final GameControllerMock mock = GameControllerMock();
          when(mock.available(any)).thenReturn(true);

          final List<FlatButton> buttons = UiHelper.buttonAlphabet(mock, Callbacks.singleArg);

          buttons[1].onPressed();
          Callbacks.assertCalled('singleArg', times: 1);
          buttons[2].onPressed();
          Callbacks.assertCalled('singleArg', withArgs: <String>['B', 'C']);
        });
      });
    });
  });
}